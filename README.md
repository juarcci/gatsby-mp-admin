<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<h1 align="center">
  <a href="https://multipase.com.ar">
    <img alt="Gatsby" src="static/MP-logo.png" width="60" />
  </a>
    Multipase dashboard: GatsbyJS +  Tabler
</h1>

Versión beta del tablero de administración implementado en el framework de generación de sitios estáticos [GatsbyJS](https://www.gatsbyjs.com). Sistema de control de acceso [Multipase](https://multipase.com.ar).

## 🚀 Quick start

1.  **Dependencias**

  - NodeJS: v17.0.1
  - Gatsby CLI: 4.24.0
  - Gatsby: 4.11.2
     ```shell
    Acá puede ir un instructivo para instalar las dependencias
    ```

2.  **Ejecución**

    Navigate into your new site’s directory and start it up.

    ```shell
    gatsby develop
    ```

    Your site is now running at http://localhost:8000!
