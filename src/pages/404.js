import React from 'react';

import { Error404Page } from "tabler-react";

export default function FourOhFourPage() {
    return <Error404Page 
        title="404" 
        subtitle="Ups... llegaste a una página de error"
        details="Disculpas pero no es posible encontrar la página que solicitaste."
        action="Volver"
    />;
}