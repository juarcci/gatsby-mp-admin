import React from 'react';
import { Site, Nav, Button, Page, Grid, Card, Table, Avatar, Text, Progress, Icon, Dropdown, Stamp } from "tabler-react";
import { Link } from 'gatsby';


export default function SpacesPage(props) {
    return (
        <Page.Content title="Personas" options={
            <Dropdown
              trigger={
                <Dropdown.Trigger
                  icon="menu"
                  toggle={true}
                  isOption={true}
                />
              }
              position="left"
              items={
                <React.Fragment>
                  <Dropdown.Item icon="tag">Action </Dropdown.Item>
                  <Dropdown.Item icon="edit-2">
                    Another action{" "}
                  </Dropdown.Item>
                  <Dropdown.Item icon="message-square">
                    Something else here
                  </Dropdown.Item>
                  <Dropdown.ItemDivider />
                  <Dropdown.Item icon="link">
                    {" "}
                    Separated link
                  </Dropdown.Item>
                </React.Fragment>
              }
            />
          }>
            <Grid.Row cards deck>
                <Grid.Col width={12}>
                <Card>
                    <Table
                    responsive
                    highlightRowOnHover
                    hasOutline
                    verticalAlign="center"
                    cards
                    className="text-nowrap"
                    >
                    <Table.Header>
                        <Table.Row>
                        <Table.ColHeader alignContent="center" className="w-1">
                            <i className="icon-people" />
                        </Table.ColHeader>
                        <Table.ColHeader><Link to="/about">Nombre</Link></Table.ColHeader>
                        <Table.ColHeader alignContent="center" className="w-1">
                            <i className="icon-people" />
                        </Table.ColHeader>
                        <Table.ColHeader>
                            <a href="#">Último acceso</a>
                        </Table.ColHeader>
                        <Table.ColHeader alignContent="center">
                            Espacios
                            <i className="icon-settings" />
                        </Table.ColHeader>
                        <Table.ColHeader alignContent="center">
                            Llaves
                            <i className="icon-settings" />
                        </Table.ColHeader>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        <Table.Row>
                        <Table.Col alignContent="center">
                            {/* <Icon payment name="visa" /> */}
                            <Avatar
                            imageURL="die.png"
                            status="green"
                            size="lg"
                            />
                        </Table.Col>
                        <Table.Col>
                            <div>Diego Ricci</div>
                            
                            <Text size="sm" muted>
                            ricci.die@gmail.com
                            </Text>
                            <Text.Small size="sm" muted>
                            <a href="#">Cambiar estado</a>
                            </Text.Small>
                        </Table.Col>
    {/*                       <Table.Col>
                            <div className="clearfix">
                            <div className="float-left">
                                <strong>42%</strong>
                            </div>
                            <div className="float-right">
                                <Text.Small muted>
                                Jun 11, 2015 - Jul 10, 2015
                                </Text.Small>
                            </div>
                            </div>
                            <Progress size="xs">
                            <Progress.Bar color="yellow" width={42} />
                            </Progress>
                        </Table.Col> */}
                        <Table.Col alignContent="center">
    {/*                         <Avatar
                            imageURL="demo/faces/female/26.jpg"
                            className="d-block"
                            status="green"
                            /> */}
                            <Stamp color="primary" icon="users"/>
                        </Table.Col>
                        <Table.Col>
                        <div>Aula 25</div>
                            <Text size="sm" muted>
                            Abierto | En uso
                            </Text>
                            <Text.Small size="sm" muted>
                            Hace 13 minutos
                            </Text.Small>
                        </Table.Col>
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        
                        {/* <Table.Col alignContent="center">42%</Table.Col> */}
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        </Table.Row>

                        <Table.Row>
                        <Table.Col alignContent="center">
                            {/* <Icon payment name="visa" /> */}
                            <Avatar
                            imageURL="chechu.png"
                            status="green"
                            size="lg"
                            />
                        </Table.Col>
                        <Table.Col>
                            <div>Lorem Ipsum</div>
                            
                            <Text size="sm" muted>
                            lorem.ipsum@loremipsum.edu
                            </Text>
                            <Text.Small size="sm" muted>
                            <a href="#">Cambiar estado</a>
                            </Text.Small>
                        </Table.Col>
    {/*                       <Table.Col>
                            <div className="clearfix">
                            <div className="float-left">
                                <strong>42%</strong>
                            </div>
                            <div className="float-right">
                                <Text.Small muted>
                                Jun 11, 2015 - Jul 10, 2015
                                </Text.Small>
                            </div>
                            </div>
                            <Progress size="xs">
                            <Progress.Bar color="yellow" width={42} />
                            </Progress>
                        </Table.Col> */}
                        <Table.Col alignContent="center">
    {/*                         <Avatar
                            imageURL="demo/faces/female/26.jpg"
                            className="d-block"
                            status="green"
                            /> */}
                            <Stamp color="green" icon="home"/>
                        </Table.Col>
                        <Table.Col>
                        <div>Aula 12</div>
                            <Text size="sm" muted>
                            Cerrado | Disponible
                            </Text>
                            <Text.Small size="sm" muted>
                            Hace 1 minuto
                            </Text.Small>
                        </Table.Col>

                        {/* <Table.Col alignContent="center">42%</Table.Col> */}
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>

                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        </Table.Row>

                        <Table.Row>
                        <Table.Col alignContent="center">
                            {/* <Icon payment name="visa" /> */}
                            <Avatar
                            imageURL="mingo.png"
                            status="green"
                            size="lg"
                            />
                        </Table.Col>
                        <Table.Col>
                            <div>Lorem Ipsum</div>
                            
                            <Text size="sm" muted>
                            lorem.ipsum@loremipsum.edu
                            </Text>
                            <Text.Small size="sm" muted>
                            <a href="#">Cambiar estado</a>
                            </Text.Small>
                        </Table.Col>
    {/*                       <Table.Col>
                            <div className="clearfix">
                            <div className="float-left">
                                <strong>42%</strong>
                            </div>
                            <div className="float-right">
                                <Text.Small muted>
                                Jun 11, 2015 - Jul 10, 2015
                                </Text.Small>
                            </div>
                            </div>
                            <Progress size="xs">
                            <Progress.Bar color="yellow" width={42} />
                            </Progress>
                        </Table.Col> */}
                        <Table.Col alignContent="center">
    {/*                         <Avatar
                            imageURL="demo/faces/female/26.jpg"
                            className="d-block"
                            status="green"
                            /> */}
                            <Stamp color="orange" icon="triangle"/>
                        </Table.Col>
                        <Table.Col>
                        <div>Aula 500</div>
                            <Text size="sm" muted>
                            Pendiente de abrir
                            </Text>
                            <Text.Small size="sm" muted>
                            Hace 8 minutos
                            </Text.Small>
                        </Table.Col>
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        {/* <Table.Col alignContent="center">42%</Table.Col> */}
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        </Table.Row>

                        <Table.Row>
                        <Table.Col alignContent="center">
                            {/* <Icon payment name="visa" /> */}
                            <Avatar
                            imageURL="pp.jpg"
                            status="green"
                            size="lg"
                            />
                        </Table.Col>
                        <Table.Col>
                            <div>Lorem Ipsum</div>
                            
                            <Text size="sm" muted>
                            lorem.ipsum@loremipsum.edu
                            </Text>
                            <Text.Small size="sm" muted>
                            <a href="#">Cambiar estado</a>
                            </Text.Small>
                        </Table.Col>
    {/*                       <Table.Col>
                            <div className="clearfix">
                            <div className="float-left">
                                <strong>42%</strong>
                            </div>
                            <div className="float-right">
                                <Text.Small muted>
                                Jun 11, 2015 - Jul 10, 2015
                                </Text.Small>
                            </div>
                            </div>
                            <Progress size="xs">
                            <Progress.Bar color="yellow" width={42} />
                            </Progress>
                        </Table.Col> */}
                        <Table.Col alignContent="center">
    {/*                         <Avatar
                            imageURL="demo/faces/female/26.jpg"
                            className="d-block"
                            status="green"
                            /> */}
                            <Stamp color="red" icon="square"/>
                        </Table.Col>
                        <Table.Col>
                        <div>Aula 501</div>
                            <Text size="sm" muted>
                            Pendiente de cerrar
                            </Text>
                            <Text.Small size="sm" muted>
                            Hace 5 minutos
                            </Text.Small>
                        </Table.Col>
                        {/* <Table.Col alignContent="center">42%</Table.Col> */}
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        </Table.Row>

                        <Table.Row>
                        <Table.Col alignContent="center">
                            {/* <Icon payment name="visa" /> */}
                            <Avatar
                            imageURL="admin.png"
                            status="green"
                            size="lg"
                            />
                        </Table.Col>
                        <Table.Col>
                            <div>Lorem Ipsum</div>
                            
                            <Text size="sm" muted>
                            lorem.ipsum@loremipsum.edu
                            </Text>
                            <Text.Small size="sm" muted>
                            <a href="#">Cambiar estado</a>
                            </Text.Small>
                        </Table.Col>
    {/*                       <Table.Col>
                            <div className="clearfix">
                            <div className="float-left">
                                <strong>42%</strong>
                            </div>
                            <div className="float-right">
                                <Text.Small muted>
                                Jun 11, 2015 - Jul 10, 2015
                                </Text.Small>
                            </div>
                            </div>
                            <Progress size="xs">
                            <Progress.Bar color="yellow" width={42} />
                            </Progress>
                        </Table.Col> */}
                        <Table.Col alignContent="center">
    {/*                         <Avatar
                            imageURL="demo/faces/female/26.jpg"
                            className="d-block"
                            status="green"
                            /> */}
                            <Stamp color="primary" icon="users"/>
                        </Table.Col>
                        <Table.Col>
                        <div>Aula 333</div>
                            <Text size="sm" muted>
                            Abierto | En uso
                            </Text>
                            <Text.Small size="sm" muted>
                            Hace 29 minutos
                            </Text.Small>
                        </Table.Col>
                        {/* <Table.Col alignContent="center">42%</Table.Col> */}
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        <Table.Col alignContent="center">
                            <Dropdown
                            trigger={
                                <Dropdown.Trigger
                                icon="more-vertical"
                                toggle={false}
                                />
                            }
                            position="right"
                            items={
                                <React.Fragment>
                                <Dropdown.Item icon="tag">Action </Dropdown.Item>
                                <Dropdown.Item icon="edit-2">
                                    Another action{" "}
                                </Dropdown.Item>
                                <Dropdown.Item icon="message-square">
                                    Something else here
                                </Dropdown.Item>
                                <Dropdown.ItemDivider />
                                <Dropdown.Item icon="link">
                                    {" "}
                                    Separated link
                                </Dropdown.Item>
                                </React.Fragment>
                            }
                            />
                        </Table.Col>
                        </Table.Row>

                    </Table.Body>
                    </Table>
                </Card>
                </Grid.Col>
          </Grid.Row>


          
        </Page.Content>
    )
}