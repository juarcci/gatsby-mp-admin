import React from 'react';
import { Site } from "tabler-react";

const navBarItems = [
    {
      value: "Espacios",
      to: "/spaces",
      icon: "home",
      // LinkComponent: withRouter(NavLink),
      useExact: true,
    },
    {
      value: "Personas",
      to: "/people",
      icon: "users",
  /*     subItems: [
        {
          value: "Cards Design",
          to: "/cards",
          LinkComponent: withRouter(NavLink),
        },
        { value: "Charts", to: "/charts", LinkComponent: withRouter(NavLink) },
        {
          value: "Pricing Cards",
          to: "/pricing-cards",
          LinkComponent: withRouter(NavLink),
        },
      ], */
    }
  ];
  
  const accountDropdownProps = {
    avatarURL: "admin.png",
    name: "Lorem Ipsum",
    description: "Administrador",
    options: [
      { icon: "user", value: "Profile" },
      { icon: "settings", value: "Settings" },
      { icon: "mail", value: "Inbox", badge: "6" },
      { icon: "send", value: "Message" },
      { isDivider: true },
      { icon: "help-circle", value: "Need help?" },
      { icon: "log-out", value: "Sign out" },
    ],
  };

export default function Layout({ children }) {
    return (
<Site.Wrapper 
      headerProps={{
        href: "/",
        alt: "Multipase",
        imageURL: "brand-svg.svg",
/*         navItems: (
          <Nav.Item type="div" className="d-none d-md-flex">
            <Button
              href="https://www.multipase.com.ar"
              target="_blank"
              outline
              size="sm"
              RootComponent="a"
              color="primary"
            >
              Ayuda
            </Button>
          </Nav.Item>
        ), */
  /*       notificationsTray: {
          notificationsObjects,
          markAllAsRead: () =>
            this.setState(
              () => ({
                notificationsObjects: this.state.notificationsObjects.map(
                  v => ({ ...v, unread: false })
                ),
              }),
              () =>
                setTimeout(
                  () =>
                    this.setState({
                      notificationsObjects: this.state.notificationsObjects.map(
                        v => ({ ...v, unread: true })
                      ),
                    }),
                  5000
                )
            ),
          unread: unreadCount,
        }, */
        accountDropdown: accountDropdownProps, 
      }} 
      navProps={{ itemsObjects: navBarItems }}>
          { children }
      </Site.Wrapper>
    );
}